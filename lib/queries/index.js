import characters from "~lib/queries/characters";
import character from "~lib/queries/character";

export default {
  ...characters,
  ...character,
};
