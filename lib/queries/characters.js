import { gql } from "@apollo/client";

const GET_CHARACTERS = gql`
  query GET_CHARACTERS($page: Int = 0, $filter: FilterCharacter) {
    characters(page: $page, filter: $filter) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        status
        species
        type
        gender
        image
        created
      }
    }
  }
`;

export default {
  GET_CHARACTERS,
};
