import { useMemo } from "react";

import {
  ApolloClient,
  HttpLink,
  ApolloLink,
  InMemoryCache,
} from "@apollo/client";

import { onError } from "@apollo/client/link/error";

global.fetch = require("node-fetch");

let globalApolloClient;

function createIsomorphLink() {
  return new HttpLink({
    uri: process.env.API,
    credentials: "same-origin",
  });
}

const errorLink = onError(({ networkError, graphQLErrors }) => {
  if (graphQLErrors) {
    graphQLErrors.map((err) => {
      console.warn(err.message);
    });
  }
  if (networkError) {
    console.warn(networkError);
  }
});

const httpLink = ApolloLink.from([errorLink, createIsomorphLink()]);

export function createApolloClient() {
  const ssrMode = typeof window === "undefined";
  const cache = new InMemoryCache({
    addTypename: false,
  });

  return new ApolloClient({
    ssrMode,
    link: httpLink,
    cache,
    connectToDevTools: process.env.NODE_ENV !== "production",
  });
}

export function initializeApollo(initialState = null) {
  const _apolloClient = globalApolloClient ?? createApolloClient();

  if (initialState) {
    const existingCache = _apolloClient.extract();
    _apolloClient.cache.restore({ ...existingCache, ...initialState });
  }

  if (typeof window === "undefined") return _apolloClient;

  if (!globalApolloClient) globalApolloClient = _apolloClient;

  return _apolloClient;
}

export function useApollo(initialState) {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
}
