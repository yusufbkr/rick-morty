import { useState, useEffect } from "react";
import Link from "next/link";

import { initializeApollo } from "~/lib/apollo";
import queries from "~/lib/queries";

import Head from "~components/Head";
import Layout from "~components/Layout";

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  const apolloClient = initializeApollo();

  const { data: { character } = {} } = await apolloClient.query({
    query: queries.GET_CHARACTER,
    variables: {
      id,
    },
  });

  if (!character) {
    return {
      redirect: {
        destination: "/characters",
        permanent: false,
      },
    };
  }

  return {
    props: {
      data: character,
    },
  };
}

export default function Index({ router: { query: { id } = {} } = {}, data }) {
  const [character, setCharacter] = useState(data);

  useEffect(() => {
    if (data) {
      setCharacter(data);
    }
  }, [id]);

  return (
    <Layout>
      <Head title={character?.name} image={character?.image} />
      <div className="container">
        <div className="page">
          <div className="mt-3">
            <Link href="/characters">← Back to characters</Link>
          </div>
          <div className="d-flex flex-column align-items-center">
            <div className="detail-card mt-3">
              <div className="text-center">
                <img
                  src={character?.image}
                  alt={character?.name}
                  className="img-fluid"
                />
              </div>
              <div>
                <h1 className="mt-3">{character?.name}</h1>
                <div className="d-flex">
                  <div className="d-flex flex-column">
                    <b>Gender: </b>
                    <b>Species: </b>
                    <b>Status: </b>
                    <b>Type: </b>
                  </div>
                  <div className="d-flex flex-column ml-2">
                    <div>{character?.gender}</div>
                    <div>{character?.species}</div>
                    <div>{character?.status}</div>
                    <div>{character?.type}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
