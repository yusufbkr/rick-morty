import { ApolloProvider } from "@apollo/client";
import { useApollo } from "~/lib/apollo";

import Router from "next/router";
import NProgress from "nprogress";

import ActionProvider from "~components/ActionProvider";

import "./app.scss";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

const Index = ({ Component, pageProps = {}, router }) => {
  const apolloClient = useApollo(pageProps.initialApolloState);

  const props = {
    ...pageProps,
    router,
  };

  return (
    <ApolloProvider client={apolloClient}>
      <ActionProvider>
        <Component {...props} />
      </ActionProvider>
    </ApolloProvider>
  );
};

export default Index;
