import { useEffect, useState, useRef } from "react";
import Link from "next/link";

import queries from "~/lib/queries";
import { useLazyQuery } from "@apollo/client";
import { useActions } from "~components/ActionProvider";

import Head from "~components/Head";
import Layout from "~components/Layout";
import Card from "~components/Card";
import Button from "~components/Button";
import Filters from "~components/Filters";

const Characters = () => {
  const [state] = useActions();
  const { name, status, species, type, gender } = state?.form || {};

  const [characters, setCharacters] = useState([]);
  const [resetCharacters, setResetCharacters] = useState(false);

  const [
    getCharacters,
    {
      data: {
        characters: { results: newCharacters = [], info = {} } = {},
      } = {},
      loading: getCharactersLoading,
    },
  ] = useLazyQuery(queries.GET_CHARACTERS);

  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);

  const getCharactersFunc = () =>
    getCharacters({
      variables: {
        page,
        filter: {
          name,
          status,
          species,
          type,
          gender,
        },
      },
    });

  useEffect(() => {
    if (resetCharacters) {
      setCharacters([]);
      setResetCharacters(false);
    }

    if (newCharacters?.length) {
      setTotalPage(info?.pages || 1);
      setCharacters([...(resetCharacters ? [] : characters), ...newCharacters]);
    }
  }, [getCharactersLoading, newCharacters?.length]);

  useEffect(() => {
    if (page !== info?.next) {
      setPage(1);
      setResetCharacters(true);
    }

    getCharactersFunc();
  }, [page, name, status, species, type, gender]);

  return (
    <Layout>
      <Head title={"Characters"} />

      <div className="page">
        <div className="container">
          <div className="row">
            <div className="col-lg-3">
              <Filters />
            </div>
            <div className="col-lg-9">
              <div className="row">
                {characters.map((character) => (
                  <div key={character.id} className="col-6 col-lg-3">
                    <Link
                      href="/character/[id]"
                      as={`/character/${character.id}`}>
                      <a>
                        <Card {...character} />
                      </a>
                    </Link>
                  </div>
                ))}
              </div>
              {!characters?.length && (
                <h4 className="text-center">Not Found</h4>
              )}
              {!!characters?.length && page < totalPage - 1 && (
                <div className="d-flex justify-content-center mt-3 mb-5">
                  <Button
                    disabled={getCharactersLoading}
                    onClick={() => setPage(page + 1)}>
                    Load More
                  </Button>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Characters;
