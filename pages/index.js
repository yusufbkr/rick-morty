import Head from "~components/Head";
import Layout from "~components/Layout";
import Hero from "~components/Hero";

const Index = () => {
  return (
    <Layout>
      <Head title={"Homepage"} />
      <Hero />
    </Layout>
  );
};

export default Index;
