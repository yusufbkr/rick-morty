import cx from "classnames";

const Input = ({
  type = "text",
  label,
  placeholder,
  name,
  value = "",
  onChange = () => {},
  disabled,
  className,
  ...others
}) => {
  return (
    <div className={cx(className, "input", "mb-3")}>
      <label htmlFor={name} className="form-label">
        {label}
      </label>
      <input
        className="form-control"
        type={type}
        name={name}
        id={name}
        placeholder={placeholder}
        value={value || ""}
        onChange={({ target: { value } = {} }) => onChange(value)}
        disabled={disabled}
        {...others}
      />
    </div>
  );
};

export default Input;
