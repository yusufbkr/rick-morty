import Head from 'next/head';
import { NextSeo } from 'next-seo';

function HeadTags({ title, description, metaImage }) {
  return (
    <>
      <NextSeo
        title={title}
        description={description}
        image={metaImage}
        twitter={{
          handle: '@handle',
          site: '@site',
          cardType: 'summary_large_image'
        }}
      />
      <Head>
        <meta property="og:image" content={metaImage} key="og:image" />
        <meta property="twitter:image" content={metaImage} />
      </Head>
    </>
  );
}

export default HeadTags;
