import { useActions } from "~components/ActionProvider";
import filterData from "~lib/initialFilterData";

import Input from "~components/Input";
import Select from "~components/Select";

function Filters() {
  const [state, setState] = useActions();
  const { name, status, species, type, gender } = state?.form || {};

  const statusMap = filterData.status.map((value) => ({ label: value, value }));
  const speciesMap = filterData.species.map((value) => ({
    label: value,
    value,
  }));
  const genderMap = filterData.gender.map((value) => ({ label: value, value }));
  const typeMap = filterData.type.map((value) => ({ label: value, value }));

  return (
    <div className="filters">
      <h5 className="mb-3">Filters</h5>
      <Input
        label="Search"
        placeholder="type name"
        value={name}
        onChange={(value) =>
          setState({ form: { ...(state?.form || {}), name: value } })
        }
      />
      <Select
        label="Status"
        value={status}
        options={[{ label: "Select", value: "" }, ...statusMap]}
        onChange={(value) =>
          setState({ form: { ...(state?.form || {}), status: value } })
        }
      />
      <Select
        label="Species"
        value={species}
        options={[{ label: "Select", value: "" }, ...speciesMap]}
        onChange={(value) =>
          setState({ form: { ...(state?.form || {}), species: value } })
        }
      />
      <Select
        label="Gender"
        value={gender}
        options={[{ label: "Select", value: "" }, ...genderMap]}
        onChange={(value) =>
          setState({ form: { ...(state?.form || {}), gender: value } })
        }
      />
      <Select
        label="Type"
        value={type}
        options={[{ label: "Select", value: "" }, ...typeMap]}
        onChange={(value) =>
          setState({ form: { ...(state?.form || {}), type: value } })
        }
      />
    </div>
  );
}

export default Filters;
