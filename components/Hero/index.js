import { useState } from "react";
import Link from "next/link";
import Router from "next/router";

import { useActions } from "~components/ActionProvider";
import Input from "~components/Input";
import Button from "~components/Button";

function Hero() {
  const [state, setState] = useActions();

  return (
    <div className="hero-module">
      <img className="hero-image" src="/images/welcome.png" />
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="full-height d-flex justify-content-lg-center flex-column justify-content-end">
              <h1 className="mb-3">Welcome!!</h1>
              <Input
                placeholder="type name"
                label={
                  <>
                    Search character or{" "}
                    <Link href="/characters">go to character list</Link>
                  </>
                }
                value={state?.form?.name || ""}
                onChange={(value) =>
                  setState({ form: { ...(state?.form || {}), name: value } })
                }
              />
              <Button
                className="mb-5 mb-lg-0"
                disabled={(state?.form?.name?.length || 0) < 3}
                onClick={() => Router.push(`/characters`)}>
                Search
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Hero;
