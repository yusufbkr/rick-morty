const Card = ({ name, image }) => {
  return (
    <div className="card">
      <div className="avatar">
        <img src={image} alt={name} />
      </div>
      <div className="name">{name}</div>
    </div>
  );
};

export default Card;
