import { createContext, useState, useContext } from "react";

const ActionContext = createContext();

const ActionProvider = ({ children }) => {
  const [state, setState] = useState({});

  const customSetState = (options = {}) => setState({ ...state, ...options });

  return (
    <ActionContext.Provider value={[state, customSetState]}>
      {children}
    </ActionContext.Provider>
  );
};
const useActions = () => useContext(ActionContext) || {};

export { ActionProvider as default, useActions };
