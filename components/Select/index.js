import cx from "classnames";

const Select = ({
  label,
  name,
  value,
  options,
  onChange,
  className,
  disabled,
}) => {
  return (
    <div className={cx(className, "select", "mb-3")}>
      <label htmlFor={name} className="form-label">
        {label}
      </label>
      <select
        className="form-control"
        name={name}
        id={name}
        onChange={({ target: { value } = {} }) => onChange(value)}
        value={value || ""}
        disabled={disabled}>
        {options.map((item, i) => (
          <option key={i} value={item.value}>
            {item.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;
