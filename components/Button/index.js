import cx from "classnames";

const Button = ({
  className,
  onClick,
  disabled,
  children,
  type = "primary",
  small = false,
}) => (
  <button
    className={cx(
      className,
      "btn",
      type && `btn-${type}`,
      small && `btn-small`,
      disabled && `disabled`,
    )}
    onClick={onClick}
    disabled={disabled}>
    {children}
  </button>
);

export default Button;
