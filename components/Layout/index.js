import Link from "next/link";

function Layout({ children }) {
  return (
    <>
      <header className="header">
        <div className="container">
          <div className="logo">
            <Link href="/">
              <a>Rick & Morty</a>
            </Link>
          </div>
        </div>
      </header>
      <main>{children}</main>
    </>
  );
}

export default Layout;
