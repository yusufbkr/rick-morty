require('dotenv').config();

const withPlugins = require('next-compose-plugins');
const withPWA = require('next-pwa');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true'
});

const nextConfiguration = {
  env: {
    API: process.env.API
  },
  images: {
    domains: ['rickandmortyapi.com']
  }
};

module.exports = withPlugins(
  [
    withBundleAnalyzer,
    [
      withPWA,
      {
        pwa: {
          disable: process.env.NODE_ENV !== 'production',
          dest: 'public'
        }
      }
    ]
  ],
  nextConfiguration
);
